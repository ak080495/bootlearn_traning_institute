from django.db import models

# Create your models here.

class SendMail(models.Model):
    name=models.CharField(max_length=200,blank=True,null=True)
    email=models.CharField(max_length=120,blank=True,null=True)
    mobile=models.CharField(max_length=120,blank=True,null=True)
    subject=models.CharField(max_length=120,blank=True,null=True)
    message=models.TextField(blank=True,null=True)
            