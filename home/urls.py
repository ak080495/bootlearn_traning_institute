from django.urls import include, path
from .import views

urlpatterns = [
   path('', views.index , name='index'),
   path('about', views.about , name='about'),
   path('pricing', views.pricing , name='pricing'),
   path('service', views.service , name='service'),
   path('blog_grid', views.blog_grid , name='blog_grid'),
   path('blog_single', views.blog_single , name='blog_single'),
   path('blog_sidebar', views.blog_sidebar , name='blog_sidebar'),
   path('contact', views.contact , name='contact'),
   path('project', views.project , name='project'),




#course
   path('python', views.python , name='python'),
   path('django', views.django , name='django'),
   path('serviceNow', views.serviceNow , name='serviceNow'),
   path('anglr', views.anglr , name='anglr'),
   path('ML', views.ML , name='ML'),







]
