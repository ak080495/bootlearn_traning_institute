from django import forms

from  .models import SendMail


class SendMailForm(forms.ModelForm):
    class Meta:
        model=SendMail
        fields=["name","email","mobile","subject","message"]